import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListClientComponent} from './list-client/list-client.component';
import {NewClientComponent} from './new-client/new-client.component';
import {EditClientComponent} from './edit-client/edit-client.component';


const routes: Routes = [
  { path: '', component: ListClientComponent },
  { path: 'new', component: NewClientComponent },
  { path: 'edit/:id', component: EditClientComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
