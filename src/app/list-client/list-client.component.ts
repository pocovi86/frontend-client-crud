import {Component, OnInit} from '@angular/core';
import {ClientService} from '../service/client.service';
import {Client} from '../models/client';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-client',
  templateUrl: './list-client.component.html',
  styleUrls: ['./list-client.component.css']
})
export class ListClientComponent implements OnInit {
  clients: Client[] = [];
  clickMessage = '';

  constructor(
    private service: ClientService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.loadClient();
  }

  loadClient(): void {
    this.service.list().subscribe(
      data => {
        this.clients = data;
      }, err => {
        console.log(err);
      });
  }

  remove(id: number) {
    this.service.delete(id).subscribe(
      data => {
        this.toastr.show('Client remove', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.loadClient();
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
      }
    );
  }

  onClickMe(id: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to delete the Client?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!'
    }).then((result) => {
        this.remove(id);
    });
  }
}
