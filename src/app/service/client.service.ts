import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Client} from '../models/client';
import {ClientReq} from '../models/clientReq';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  clientUrl = 'http://localhost:8080/api/';

  constructor(private httpClient: HttpClient) {
  }

  // metodo backend
  public list(): Observable<Client[]> {
    return this.httpClient.get<Client[]>(this.clientUrl + 'listClient');
  }

  public detail(id: number): Observable<Client> {
    return this.httpClient.get<Client>(this.clientUrl + `detail/${id}`);
  }

  public save(client: ClientReq): Observable<any> {
    return this.httpClient.post<any>(this.clientUrl + 'create', client);
  }

  public update(id: number, client: ClientReq): Observable<any> {
    return this.httpClient.put<any>(this.clientUrl + `update/${id}`, client);
  }
  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.clientUrl + `delete/${id}`);
  }
}
