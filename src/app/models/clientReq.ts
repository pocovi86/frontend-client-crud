import {City} from './city';

export class ClientReq {
  id: number;
  name: string;
  lastName: string;
  phone: string;
  email: string;
  cityName: string;

  constructor(name: string, lastName: string, phone: string, email: string, cityName: string) {
    this.name = name;
    this.lastName = lastName;
    this.phone = phone;
    this.email = email;
    this.cityName = cityName;
  }
}
