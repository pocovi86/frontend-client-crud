export class City{
  id: number;
  cityName: string;

  constructor(cityName: string) {
    this.cityName = cityName;
  }
}
