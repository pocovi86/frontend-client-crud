import {City} from './city';

export class Client {
  id: number;
  name: string;
  lastName: string;
  phone: string;
  email: string;
  city: City;

  constructor(name: string, lastName: string, phone: string, email: string, city: City) {
    this.name = name;
    this.lastName = lastName;
    this.phone = phone;
    this.email = email;
    this.city = city;
  }
}
