import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {ClientService} from '../service/client.service';
import {ClientReq} from '../models/clientReq';
import {Client} from '../models/client';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {
  client: Client = null;
  datos: string[] = ['San Salvador de Jujuy', 'Palpalá', 'Perico', 'Monterrico', 'Abra Pampa', 'San Antonio', 'Volcán'];

  constructor(
    private service: ClientService,
    private toastr: ToastrService,
    private activateRoute: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    const id = this.activateRoute.snapshot.params.id;
    this.service.detail(id).subscribe(
      data => {
        this.client = data;
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/']);
      });
  }

  onUpdate(): void {
    const id = this.activateRoute.snapshot.params.id;
    const customer = new ClientReq(this.client.name, this.client.lastName, this.client.phone, this.client.email, this.client.city.cityName);
    this.service.update(id, customer).subscribe(
      data => {
        this.toastr.success('Product updated', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/']);
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/']);
      });
  }

}
