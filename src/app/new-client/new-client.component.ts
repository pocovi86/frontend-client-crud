import {Component, OnInit} from '@angular/core';
import {ClientService} from '../service/client.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {Client} from '../models/client';
import {City} from '../models/city';
import {ClientReq} from '../models/clientReq';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.css']
})
export class NewClientComponent implements OnInit {
  datos: string[] = ['San Salvador de Jujuy', 'Palpalá', 'Perico', 'Monterrico', 'Abra Pampa', 'San Antonio', 'Volcán'];
  name = '';
  lastName = '';
  phone = '';
  email = '';
  city = '';

  constructor(
    private service: ClientService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
  }

  onCreate(): void {
    const city = new City(this.city);
    const customer = new Client(this.name, this.lastName, this.phone, this.email, city);
    const client = new ClientReq(customer.name, customer.lastName, customer.phone, customer.email, customer.city.cityName);
    this.service.save(client).subscribe(
      data => {
        /*this.toastr.success('Client created', 'OK', {timeOut: 3000, positionClass: 'toast-top-center'});*/
        Swal.fire({
          position: 'top-right',
          icon: 'success',
          title: 'Client created',
          showConfirmButton: false,
          timer: 1800
        });
        this.router.navigate(['/']);
      }, err => {
        this.toastr.error(err.error.message, 'Fail', {timeOut: 3000, positionClass: 'toast-top-center'});
        this.router.navigate(['/']);
      });
  }
}
